<?php include '_header.php'; ?>
<div class="content txt-center has-menu">
	<div class="content-menu clearfix">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-third"><a href="a-mol.php" class="content-menu-link icon-historia active"><span class="inner-icon"></span>Nossa História</a></li>
					<li class="content-menu-item one-third"><a href="3-pilares.php" class="content-menu-link icon-pilares"><span class="inner-icon"></span>3 Pilares de Qualidade</a></li>
					<li class="content-menu-item one-third"><a href="equipe.php" class="content-menu-link icon-equipe"><span class="inner-icon"></span>Equipe</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section>
		<div class="container_12 clearfix">
			<h2>Nossa História</h2>
			<div class="timeline">
				<div class="grid_8 push_2 years carousel-years">
					<div class="year">
						<p>2005 - 2010</p>
					</div>
					<div class="year">
						<p>2011</p>
					</div>
					<div class="year">
						<p>2012</p>
					</div>
					<div class="year">
						<p>2013</p>
					</div>
					<div class="year">
						<p>2014</p>
					</div>
				</div>
				<div class="grid_10 push_1 carousel-facts">
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2005 - 2010</h3>
						<p>Formação dos Sócio-fundadores na Escola de Engenharia da UFG e participação efetiva no Centro Acadêmico de Engenharia Elétrica e fundação da Atlética da Engenharia Elétrica UFG e da ELO Engenharia Júnior, primeira empresa júnior de engenharias da UFG;</p>
					</div>
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2011</h3>
						<p>Fundação da Mol! Engenharia e início das atividades em desenvolvimento de Projetos de Instalações Elétricas</p>
					</div>
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2012</h3>
						<p>Inclusão das disciplinas de engenharia civil no portfólio de serviços e início das atividades de projetos em Instalações Hidrossanitárias e Combate à Incêndio e inclusão da implementação dos processos BIM no Planejamento Estratégico da empresa</p>
					</div>
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2013</h3>
						<p>Capacitação e treinamento nos softwares de modelagem, implementação dos processos de modelagem paramétrica (3D), início da criação do banco de famílias Mol! e desenvolvimentos dos primeiros cases de instalações prediais em 3D</p>
					</div>
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2014</h3>
						<p>Consolidação e melhoria dos processos, início do desenvolvimento dos produtos 4D. Reformulação da identidade visual e posicionamento de marca tendo em vista o pioneirismo nos processos em BIM.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include '_footer.php'; ?>