<?php 
$page = "bim";
include '_header.php'; ?>
<div class="content has-menu">
	<div class="content-menu clearfix txt-center">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-quarter"><a href="entenda-o-bim.php" class="content-menu-link icon-entenda-bim"><span class="inner-icon"></span>Entenda o BIM</a></li>
					<li class="content-menu-item one-quarter"><a href="por-que-bim.php" class="content-menu-link icon-por-que-bim active"><span class="inner-icon"></span>Por que BIM?</a></li>
					<li class="content-menu-item one-quarter"><a href="pense-mol-pense-bim.php" class="content-menu-link icon-pense-bim"><span class="inner-icon"></span>pense mol! pense bim</a></li>
					<li class="content-menu-item one-quarter"><a href="construa-com-bim.php" class="content-menu-link icon-construa"><span class="inner-icon"></span>Construa com BIM</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section>
		<div class="container_12 clearfix">
			<div class="topic clearfix">
				<div class="grid_4">
					<h2>Instalações Prediais em 3D</h2>
					<a href="" title="vídeos" class="icon-video read-more js-open-lightbox" data-images="https://www.youtube.com/watch?v=SvtVU4QWRjQ;https://www.youtube.com/watch?v=8iLw_iAS6uM;" data-titles="sobre o bim;o que é o bim;" data-descriptions="saiba mais sobre o bim;;">vídeos</a>
					<a href="" title="galeria de imagens" class="icon-gallery read-more js-open-lightbox" data-images="images/temp/header.jpg;images/temp/slide.jpg;" data-titles="imagem 1;outra imagem;" data-descriptions="descricao;;">galeria de imagens</a>
				</div>
				<div class="grid_8">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, aspernatur ipsam nobis maxime ipsum alias nisi voluptatibus sit nulla fuga quaerat quae corrupti molestiae eum rerum tempora, fugit nesciunt ducimus? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, aspernatur ipsam nobis maxime ipsum alias nisi voluptatibus sit nulla fuga quaerat quae corrupti molestiae eum rerum tempora, fugit nesciunt ducimus?</p>
				</div>
			</div>
			<div class="topic clearfix">
				<div class="grid_4">
					<h2>Instalações Prediais em 3D</h2>
					<a href="" title="vídeos" class="icon-video read-more js-open-lightbox" data-images="https://www.youtube.com/watch?v=SvtVU4QWRjQ;https://www.youtube.com/watch?v=8iLw_iAS6uM;" data-titles="sobre o bim;o que é o bim;" data-descriptions="saiba mais sobre o bim;;">vídeos</a>
					<a href="" title="galeria de imagens" class="icon-gallery read-more js-open-lightbox" data-images="images/temp/header.jpg;images/temp/slide.jpg;" data-titles="imagem 1;outra imagem;" data-descriptions="descricao;;">galeria de imagens</a>
				</div>
				<div class="grid_8">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, aspernatur ipsam nobis maxime ipsum alias nisi voluptatibus sit nulla fuga quaerat quae corrupti molestiae eum rerum tempora, fugit nesciunt ducimus? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, aspernatur ipsam nobis maxime ipsum alias nisi voluptatibus sit nulla fuga quaerat quae corrupti molestiae eum rerum tempora, fugit nesciunt ducimus?</p>
				</div>
			</div>
			<div class="topic clearfix">
				<div class="grid_4">
					<h2>Instalações Prediais em 3D</h2>
					<a href="" title="vídeos" class="icon-video read-more js-open-lightbox" data-images="https://www.youtube.com/watch?v=SvtVU4QWRjQ;https://www.youtube.com/watch?v=8iLw_iAS6uM;" data-titles="sobre o bim;o que é o bim;" data-descriptions="saiba mais sobre o bim;;">vídeos</a>
					<a href="" title="galeria de imagens" class="icon-gallery read-more js-open-lightbox" data-images="images/temp/header.jpg;images/temp/slide.jpg;" data-titles="imagem 1;outra imagem;" data-descriptions="descricao;;">galeria de imagens</a>
				</div>
				<div class="grid_8">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, aspernatur ipsam nobis maxime ipsum alias nisi voluptatibus sit nulla fuga quaerat quae corrupti molestiae eum rerum tempora, fugit nesciunt ducimus? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, aspernatur ipsam nobis maxime ipsum alias nisi voluptatibus sit nulla fuga quaerat quae corrupti molestiae eum rerum tempora, fugit nesciunt ducimus?</p>
				</div>
			</div>
		</div>
	</section>
</div>
<section class="bg-orange txt-center">
	<a href="" title="projetos bim da mol!" class="button">projetos bim da mol!<span class="go"></span></a>
</section>
<?php include '_footer.php'; ?>