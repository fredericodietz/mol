<?php 
$page = "blog";
include '_header.php'; ?>
<section class="content">
	<div class="container_12 clearfix">
		<div class="grid_12">
			<div class="box post clearfix">
					<img src="https://api.fnkr.net/testimg/385x350/00CED1/FFF/?text=imagem" class="post-thumb">
				
			<div class="post-content">
					<div class="post-info">
						<p class="post-date"><img src="images/icon-date.png" alt="">12 ago 2014</p>
						<p class="post-comments"><img src="images/icon-comments.png" alt="">5 comentários</p>
					</div>
					<h2 class="post-title">Chegou ao Brasil uma máquina capaz de extrair umidade do ar e transformá-la em água potável</h2>
					<p class="post-excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis voluptatem quaerat necessitatibus laborum deserunt recusandae sit, nesciunt ut libero, labore porro autem ipsum modi fugiat illo doloribus officia facere.</p>
					<a href="" title="continuar lendo" class="read-more">continuar lendo</a>
				</div>
			</div>
			<div class="clear"></div>
			<div class="box post clearfix">
					<img src="https://api.fnkr.net/testimg/385x350/00CED1/FFF/?text=imagem" class="post-thumb">
				
			<div class="post-content">
					<div class="post-info">
						<p class="post-date"><img src="images/icon-date.png" alt="">12 ago 2014</p>
						<p class="post-comments"><img src="images/icon-comments.png" alt="">5 comentários</p>
					</div>
					<h2 class="post-title">Chegou ao Brasil uma máquina capaz de extrair umidade do ar e transformá-la em água potável</h2>
					<p class="post-excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis voluptatem quaerat necessitatibus laborum deserunt recusandae sit, nesciunt ut libero, labore porro autem ipsum modi fugiat illo doloribus officia facere.</p>
					<a href="" title="continuar lendo" class="read-more">continuar lendo</a>
				</div>
			</div>
			<div class="clear"></div>
			<div class="box post clearfix">
					<img src="https://api.fnkr.net/testimg/385x350/00CED1/FFF/?text=imagem" class="post-thumb">
				
			<div class="post-content">
					<div class="post-info">
						<p class="post-date"><img src="images/icon-date.png" alt="">12 ago 2014</p>
						<p class="post-comments"><img src="images/icon-comments.png" alt="">5 comentários</p>
					</div>
					<h2 class="post-title">Chegou ao Brasil uma máquina capaz de extrair umidade do ar e transformá-la em água potável</h2>
					<p class="post-excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis voluptatem quaerat necessitatibus laborum deserunt recusandae sit, nesciunt ut libero, labore porro autem ipsum modi fugiat illo doloribus officia facere.</p>
					<a href="" title="continuar lendo" class="read-more">continuar lendo</a>
				</div>
			</div>
			<div class="clear"></div>
			<div class="box post clearfix">
					<img src="https://api.fnkr.net/testimg/385x350/00CED1/FFF/?text=imagem" class="post-thumb">
				
			<div class="post-content">
					<div class="post-info">
						<p class="post-date"><img src="images/icon-date.png" alt="">12 ago 2014</p>
						<p class="post-comments"><img src="images/icon-comments.png" alt="">5 comentários</p>
					</div>
					<h2 class="post-title">Chegou ao Brasil uma máquina capaz de extrair umidade do ar e transformá-la em água potável</h2>
					<p class="post-excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis voluptatem quaerat necessitatibus laborum deserunt recusandae sit, nesciunt ut libero, labore porro autem ipsum modi fugiat illo doloribus officia facere.</p>
					<a href="" title="continuar lendo" class="read-more">continuar lendo</a>
				</div>
			</div>
			<div class="clear"></div>
			<div class="wp-pagenavi">
				<span class="disabled">&#8592;</span>
				<span class="current">1</span>
				<a href="#" title="">2</a>
				<a href="#" title="">3</a>
				<a href="#" title="">4</a>
				<a href="#" title="">...</a>
				<a href="#" title="">&#8594;</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<?php include '_footer.php'; ?>