<?php
$page = "home";
include '_header.php'; ?>
<div class="content">
	<section class="how bg-image bgParallax" data-speed="1.7">
		<div class="container_12 clearfix">
			<div class="grid_4">
				<h2 class="section-title">como entregamos qualidade</h2>
			</div>
			<div class="grid_8">
				<div class="how-text">
					<p>A inclusão de um pensamento intuitivo que trará atributos como criatividade, estética, trabalho em equipe com foco no usuário das instalações projetadas.</p>
				</div>
			</div>
			<div class="clear"></div>
			<div class="how-topics">
				<div class="grid_4">
					<img src="images/icon-parceria.png" alt="">
					<h3>abordagem de parceria</h3>
					<p>A inclusão de um pensamento intuitivo que trará atributos como criatividade, estética, trabalho em equipe com foco no usuário das instalações projetadas.</p>
				</div>
				<div class="grid_4">
					<img src="images/icon-ciclo.png" alt="">
					<h3>ciclo de vida do design</h3>
					<p>A inclusão de um pensamento intuitivo que trará atributos como criatividade, estética, trabalho em equipe com foco no usuário das instalações projetadas.</p>
				</div>
				<div class="grid_4">
					<img src="images/icon-gestao.png" alt="">
					<h3>navegador de gestão</h3>
					<p>A inclusão de um pensamento intuitivo que trará atributos como criatividade, estética, trabalho em equipe com foco no usuário das instalações projetadas.</p>
				</div>
			</div>
		</div>
	</section>
	<div class="pense-bim">
		<div class="vert-center">
			<div class="container_12 clearfix">
				<div class="grid_12 txt-center">
					<img src="images/pense-mol-pense-bim.png" alt="">
					<p>A inclusão de um pensamento intuitivo que trará atributos como criatividade, estética, trabalho em equipe com foco no usuário das instalações projetadas.</p>
					<a href="" title="" class="button button-red">conheça mais sobre bim</a>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="txt-center bg-image interessado bgParallax" data-speed="1.7">
	<div class="container_12 clearfix">
		<div class="grid_12">
			<h2 class="section-title">interessado no nosso trabalho?</h2>
			<p>A inclusão de um pensamento intuitivo que trará atributos como criatividade, estética, trabalho em equipe com foco no usuário das instalações projetadas.</p>
			<a href="" title="" class="button">entre em contato</a>
		</div>
	</div>
</section>

<?php include '_footer.php'; ?>