<?php 
$contato = true;
include '_header.php'; ?>
<section class="content">
	<div class="container_12 clearfix">
		<div class="grid_12">
			<form action="" method="get">
				<div class="grid_6 alpha omega">
					<div class="grid_4 alpha">
						<label for="nome">Nome</label>
						<input type="text">
					</div>
					<div class="grid_2 alpha">
						<label for="telefone">Telefone</label>
						<input type="text">
					</div>
					<div class="grid_3 alpha">
						<label for="email">Email</label>
						<input type="text">
					</div>
					<div class="grid_3 alpha">
						<label for="empresa">Empresa</label>
						<input type="text">
					</div>
				</div>
				<div class="textarea-fix">
					<label for="msg">Mensagem</label>
					<textarea name="" style="height: 128px;"></textarea>
				</div>
				<div class="clear"></div>
				<input type="submit" name="" value="enviar" class="submit-button">
			</form>
		</div>
	</div>
</section>
<section class="be-partner txt-center">
	<div class="container_12 clearfix">
		<div class="grid_12">
			<h3>Quer ser um parceiro, cliente ou fazer parte do nosso time? Clique nas opções abaixo:</h3>
			<a href="" title="Escritórios de Arquitetura" class="button icon-escritorios-dark"><span class="inner-icon"></span>Escritórios de Arquitetura</a>
			<a href="" title="Clientes" class="button icon-clientes-dark"><span class="inner-icon"></span>Clientes</a>
			<a href="" title="Time Mol!" class="button icon-equipe-dark"><span class="inner-icon"></span>Time Mol!</a>
		</div>
	</div>
</section>
<div id="map_canvas" class="map"></div>
<?php include '_footer.php'; ?>