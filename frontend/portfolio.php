<?php 
$page = "portfolio";
include '_header.php'; ?>
<div class="content txt-center">
	<div class="portfolio-item">
		<h3 class="portfolio-title">Terra Mundi</h3>
		<span class="icon-type portfolio-info">Residencial</span>
		<span class="icon-city portfolio-info">Goiânia - GO</span>
		<span class="icon-square portfolio-info">3D BIM</span>
		<div class="cover">
			<a href="#portfolio-1" title="detalhes do projeto" class="button js-open-portfolio-lightbox">detalhes do projeto</a>
		</div>
		<div class="portfolio-content" id="portfolio-1">
			<div class="container_12">
				<div class="grid_12">
					<span class="close js-close-lightbox"><img src="images/close.png" alt=""></span>
					<div class="clear"></div>
					<div class="lightbox-content">
						<h2>Terra Mundi</h2>
						<span class="icon-type portfolio-info">Residencial</span>
						<span class="icon-city portfolio-info">Goiânia - GO</span>
						<span class="icon-square portfolio-info">3D BIM</span>
						<div class="project">
							<div class="project-carousel grid_7 alpha">
								<img src="https://api.fnkr.net/testimg/555x450/00CED1/FFF/?text=img+placeholder">
							</div>
							<div class="project-info grid_5 omega">
								<p class="project-status">Projeto em andamento</p>
								<p><strong>Contrutora e Incorporadora:</strong></p>
								<p>Merzian Construtora e Incorporadora</p>
								<p>Loft Construtora</p>
								<p><strong>Área Construída:</strong></p>
								<p>12.500m2</p>
								<p><strong>Projeto de Arquitetura:</strong></p>
								<p>ARQ URB</p>
								<p><strong>Projeto de Estrutura:</strong></p>
								<p>Bueno Projetos Estruturais</p>
								<p><strong>Serviços:</strong></p>
							<span alt="Design de subestação" class="project-feature">
								<span class="icon-hidrossanitaria-standalone"></span>
								</span>
								<span class="icon-incendio-standalone"></span>
								<span class="icon-4d-standalone"></span>
								<span class="icon-eletrica-standalone"></span>
							</div>
							<div class="clear"></div>
							<div class="grid_4 alpha">
								<p class="quote-title">Uma empresa com a abordagem dinâmica e diferenciada</p>
							</div>
							<blockquote class="grid_8 omega">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nam, voluptatem tenetur asperiores dolorum rem placeat! Fugit quidem iure, autem, labore nesciunt accusamus? Quasi ea sed alias numquam repellat rerum.</p>
								<p class="author">Júlio Braga - Coordenador de projetos Merzian Construtora e Incorporadora</p>
							</blockquote>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="portfolio-item">
		<h3 class="portfolio-title">Terra Mundi</h3>
		<span class="icon-type portfolio-info">Residencial</span>
		<span class="icon-city portfolio-info">Goiânia - GO</span>
		<span class="icon-square portfolio-info">3D BIM</span>
		<div class="cover">
			<a href="#portfolio-2" title="detalhes do projeto" class="button js-open-portfolio-lightbox">detalhes do projeto</a>
		</div>
		<div class="portfolio-content" id="portfolio-2">
			<div class="container_12">
				<div class="grid_12">
					<span class="close js-close-lightbox"><img src="images/close.png" alt=""></span>
					<div class="clear"></div>
					<div class="lightbox-content">
						<h2>Terra Mundi</h2>
						<span class="icon-type portfolio-info">Residencial</span>
						<span class="icon-city portfolio-info">Goiânia - GO</span>
						<span class="icon-square portfolio-info">3D BIM</span>
						<div class="project">
							<div class="project-carousel grid_7 alpha">
								<img src="https://api.fnkr.net/testimg/555x450/00CED1/FFF/?text=img+placeholder">
							</div>
							<div class="project-info grid_5 omega">
								<p class="project-status">Projeto em andamento</p>
								<p><strong>Contrutora e Incorporadora:</strong></p>
								<p>Merzian Construtora e Incorporadora</p>
								<p>Loft Construtora</p>
								<p><strong>Área Construída:</strong></p>
								<p>12.500m2</p>
								<p><strong>Projeto de Arquitetura:</strong></p>
								<p>ARQ URB</p>
								<p><strong>Projeto de Estrutura:</strong></p>
								<p>Bueno Projetos Estruturais</p>
								<p><strong>Serviços:</strong></p>
							<span alt="Design de subestação" class="project-feature">
								<span class="icon-hidrossanitaria-standalone"></span>
								</span>
								<span class="icon-incendio-standalone"></span>
								<span class="icon-4d-standalone"></span>
								<span class="icon-eletrica-standalone"></span>
							</div>
							<div class="clear"></div>
							<div class="grid_4 alpha">
								<p class="quote-title">Uma empresa com a abordagem dinâmica e diferenciada</p>
							</div>
							<blockquote class="grid_8 omega">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nam, voluptatem tenetur asperiores dolorum rem placeat! Fugit quidem iure, autem, labore nesciunt accusamus? Quasi ea sed alias numquam repellat rerum.</p>
								<p class="author">Júlio Braga - Coordenador de projetos Merzian Construtora e Incorporadora</p>
							</blockquote>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="portfolio-item">
		<h3 class="portfolio-title">Terra Mundi</h3>
		<span class="icon-type portfolio-info">Residencial</span>
		<span class="icon-city portfolio-info">Goiânia - GO</span>
		<span class="icon-square portfolio-info">3D BIM</span>
		<div class="cover">
			<a href="#portfolio-3" title="detalhes do projeto" class="button js-open-portfolio-lightbox">detalhes do projeto</a>
		</div>
		<div class="portfolio-content" id="portfolio-3">
			<div class="container_12">
				<div class="grid_12">
					<span class="close js-close-lightbox"><img src="images/close.png" alt=""></span>
					<div class="clear"></div>
					<div class="lightbox-content">
						<h2>Terra Mundi</h2>
						<span class="icon-type portfolio-info">Residencial</span>
						<span class="icon-city portfolio-info">Goiânia - GO</span>
						<span class="icon-square portfolio-info">3D BIM</span>
						<div class="project">
							<div class="project-carousel grid_7 alpha">
								<img src="https://api.fnkr.net/testimg/555x450/00CED1/FFF/?text=img+placeholder">
							</div>
							<div class="project-info grid_5 omega">
								<p class="project-status">Projeto em andamento</p>
								<p><strong>Contrutora e Incorporadora:</strong></p>
								<p>Merzian Construtora e Incorporadora</p>
								<p>Loft Construtora</p>
								<p><strong>Área Construída:</strong></p>
								<p>12.500m2</p>
								<p><strong>Projeto de Arquitetura:</strong></p>
								<p>ARQ URB</p>
								<p><strong>Projeto de Estrutura:</strong></p>
								<p>Bueno Projetos Estruturais</p>
								<p><strong>Serviços:</strong></p>
							<span alt="Design de subestação" class="project-feature">
								<span class="icon-hidrossanitaria-standalone"></span>
								</span>
								<span class="icon-incendio-standalone"></span>
								<span class="icon-4d-standalone"></span>
								<span class="icon-eletrica-standalone"></span>
							</div>
							<div class="clear"></div>
							<div class="grid_4 alpha">
								<p class="quote-title">Uma empresa com a abordagem dinâmica e diferenciada</p>
							</div>
							<blockquote class="grid_8 omega">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nam, voluptatem tenetur asperiores dolorum rem placeat! Fugit quidem iure, autem, labore nesciunt accusamus? Quasi ea sed alias numquam repellat rerum.</p>
								<p class="author">Júlio Braga - Coordenador de projetos Merzian Construtora e Incorporadora</p>
							</blockquote>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="wp-pagenavi">
		<span class="disabled">&#8592;</span>
		<span class="current">1</span>
		<a href="#" title="">2</a>
		<a href="#" title="">3</a>
		<a href="#" title="">4</a>
		<a href="#" title="">...</a>
		<a href="#" title="">&#8594;</a>
	</div>
	<div class="clear"></div>
</div>
<div class="lightbox">
	<div class="container_12">
		<div class="grid_12">
			<span class="close js-close-lightbox"><img src="images/close.png" alt=""></span>
			<div class="clear"></div>
			<div class="lightbox-content">
			</div>
		</div>
	</div>
</div>
<?php include '_footer.php'; ?>