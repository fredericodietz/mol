<?php 
$page = "bim";
include '_header.php'; ?>
<div class="content has-menu">
	<div class="content-menu clearfix">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-quarter"><a href="entenda-o-bim.php" class="content-menu-link icon-entenda-bim"><span class="inner-icon"></span>Entenda o BIM</a></li>
					<li class="content-menu-item one-quarter"><a href="por-que-bim.php" class="content-menu-link icon-por-que-bim"><span class="inner-icon"></span>Por que BIM?</a></li>
					<li class="content-menu-item one-quarter"><a href="pense-mol-pense-bim.php" class="content-menu-link icon-pense-bim active"><span class="inner-icon"></span>pense mol! pense bim</a></li>
					<li class="content-menu-item one-quarter"><a href="construa-com-bim.php" class="content-menu-link icon-construa"><span class="inner-icon"></span>Construa com BIM</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section>
		<div class="container_12 txt-center clearfix">
		<h2>historico-bim-na-mol</h2>
		<div class="timeline">
				<div class="grid_8 push_2 years carousel-years">
					<div class="year">
						<span>dez</span>
						<p>2012</p>
					</div>
					<div class="year">
						<span>jan</span>
						<p>2013</p>
					</div>
					<div class="year">
						<span>fev</span>
						<p>2013</p>
					</div>
					<div class="year">
						<span>jun</span>
						<p>2013</p>
					</div>
					<div class="year">
						<span>ago</span>
						<p>2014</p>
					</div>
				</div>
				<div class="grid_10 push_1 carousel-facts">
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2012</h3>
						<p>Formação dos Sócio-fundadores na Escola de Engenharia da UFG e participação efetiva no Centro Acadêmico de Engenharia Elétrica e fundação da Atlética da Engenharia Elétrica UFG e da ELO Engenharia Júnior, primeira empresa júnior de engenharias da UFG;</p>
					</div>
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2013</h3>
						<p>Fundação da Mol! Engenharia e início das atividades em desenvolvimento de Projetos de Instalações Elétricas</p>
					</div>
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2013</h3>
						<p>Inclusão das disciplinas de engenharia civil no portfólio de serviços e início das atividades de projetos em Instalações Hidrossanitárias e Combate à Incêndio e inclusão da implementação dos processos BIM no Planejamento Estratégico da empresa</p>
					</div>
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2013</h3>
						<p>Capacitação e treinamento nos softwares de modelagem, implementação dos processos de modelagem paramétrica (3D), início da criação do banco de famílias Mol! e desenvolvimentos dos primeiros cases de instalações prediais em 3D</p>
					</div>
					<div>
						<img src="images/timeline/formatura.jpg" alt="" class="year-icon">
						<h3>2014</h3>
						<p>Consolidação e melhoria dos processos, início do desenvolvimento dos produtos 4D. Reformulação da identidade visual e posicionamento de marca tendo em vista o pioneirismo nos processos em BIM.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include '_footer.php'; ?>