<?php 
$page = "blog";
include '_header.php'; ?>
<section class="content">
	<div class="container_12 clearfix">
		<article class="grid_8">
			<div class="carousel-post">
				<div><img src="images/temp/slide.jpg" alt=""></div>
				<div><img src="images/temp/slide.jpg" alt=""></div>
				<div><img src="images/temp/slide.jpg" alt=""></div>
				<div><img src="images/temp/slide.jpg" alt=""></div>
			</div>
			<h2 class="post-title">Chegou ao Brasil uma máquina capaz de extrair umidade do ar e transformá-la em água potável</h2>
			<div class="post-info">
				<p class="post-date"><img src="images/icon-date.png" alt="">12 ago 2014</p>
				<p class="post-comments"><a href=""><img src="images/icon-comments.png" alt="">5 comentários</a></p>
			</div>
			<div class="text">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque quam, harum, officiis provident nisi id labore fugiat libero commodi hic deserunt optio consequuntur ab temporibus facilis esse tempore. Maiores.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate ea a, eligendi beatae illo modi omnis. Ducimus repellat <a href="" title="">libero laboriosam dolore</a> quod blanditiis nisi molestiae, nesciunt voluptas facilis perspiciatis eos!</p>
				<ul>
					<li>Item 1</li>
					<li>item 2</li>
					<li>item com uma descrição maior</li>
					<li>outro item</li>
				</ul>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque quam, harum, officiis provident nisi id labore fugiat libero commodi hic deserunt optio consequuntur ab temporibus facilis esse tempore. Maiores.</p>
				<img src="https://api.fnkr.net/testimg/300x255/00CED1/FFF/?text=img+placeholder" class="alignleft">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate ea a, eligendi beatae illo modi omnis. Ducimus repellat libero laboriosam dolore quod blanditiis nisi molestiae, nesciunt voluptas facilis perspiciatis eos!</p>
				<p><a href="">Lorem ipsum dolor sit amet</a>, consectetur adipisicing elit. Eligendi doloremque quam, harum, officiis provident nisi id labore fugiat libero commodi hic deserunt optio consequuntur ab temporibus facilis esse tempore. Maiores.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate ea a, eligendi beatae illo modi omnis. Ducimus repellat libero laboriosam dolore quod blanditiis nisi molestiae, nesciunt voluptas facilis perspiciatis eos!</p>
				<blockquote>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis aut obcaecati dolore magni, id eos libero nobis in ipsum, dicta dolor blanditiis quo saepe facere labore. Assumenda doloribus laborum fugit!</p>
				</blockquote>
				<h3>A solução</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi doloremque quam, harum, officiis provident nisi id labore fugiat libero commodi hic deserunt optio consequuntur ab temporibus facilis esse tempore. Maiores.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate ea a, eligendi beatae illo modi omnis. Ducimus repellat libero laboriosam dolore quod blanditiis nisi molestiae, nesciunt voluptas facilis perspiciatis eos!</p>
			</div>
			<a href="" title="" class="back">voltar para postagens</a>
			<p>deixe um comentário</p>
		</article>
		<aside class="grid_4 sidebar">
			<form action="" method="" accept-charset="utf-8">
				<input type="text" name="" value="" placeholder="faça uma busca no blog" class="search-input">
				<input type="submit" name="" value="" class="search-button">
			</form>
			<h2>posts recentes</h2>
			<div class="recent-post">
				<div class="post-info">
					<p class="post-date"><img src="images/icon-date.png" alt="">12 ago 2014</p>
					<p class="post-comments"><a href=""><img src="images/icon-comments.png" alt="">5 comentários</a></p>
				</div>
				<h3 class="post-title"><a href="" title="">Chegou ao Brasil uma máquina capaz de extrair umidade do ar e transformá-la em água potável</a></h3>
			</div>
			<div class="recent-post">
				<div class="post-info">
					<p class="post-date"><img src="images/icon-date.png" alt="">12 ago 2014</p>
					<p class="post-comments"><a href=""><img src="images/icon-comments.png" alt="">5 comentários</a></p>
				</div>
				<h3 class="post-title"><a href="" title="">Chegou ao Brasil uma máquina capaz de extrair umidade do ar e transformá-la em água potável</a></h3>
			</div>
			<div class="recent-post">
				<div class="post-info">
					<p class="post-date"><img src="images/icon-date.png" alt="">12 ago 2014</p>
					<p class="post-comments"><a href=""><img src="images/icon-comments.png" alt="">5 comentários</a></p>
				</div>
				<h3 class="post-title"><a href="" title="">Chegou ao Brasil uma máquina capaz de extrair umidade do ar e transformá-la em água potável</a></h3>
			</div>


		</aside>
	</div>
</section>

<?php include '_footer.php'; ?>