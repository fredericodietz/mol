jQuery(document).ready(function($) {

	$('.carousel-clients, .carousel-partners').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 6,
		arrows: false,
		dots: true
	});

	$(".post .carousel-post").slick();

	$('.carousel-years').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: false,
		dots: false,
		arrows: false,
		asNavFor: '.carousel-facts',
		centerMode: true,
    focusOnSelect: true
	});

	$('.carousel-facts').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: false,
		fade: true,
		arrows: true,
		asNavFor: '.carousel-years',
		centerMode: false
	});

	// var originalImage;
	// $(".js-flip").hover(function() {
	// 	$(this).find(".js-flip-image").fadeOut();
	// 	$(this).find(".flip-cover").fadeIn();
	// }, function() {
	// 	$(this).find(".flip-cover").fadeOut();
	// 	$(this).find(".js-flip-image").fadeIn();
	// });

	var windowHeight = $(window).height(),
      katon = $(".assinatura-katon"),
			footerOffsetTop = $(".main-footer").offset().top,
			contentTop = $(".content").offset().top;
	$(window).scroll(function(event){
		if($(window).scrollTop() > 180) {
			$(".header-transparent").addClass('header-white');
		} else {
			$(".header-transparent").removeClass('header-white');
		}
    // if($(window).scrollTop() >= (footerOffsetTop - windowHeight + 125)) {
    //   katon.css("right", "-4px");
    // } else {
    //   katon.css("right", "-82px");
    // }
	});

	$(".home .main-header, .video").css("height", $(window).height() + "px");

	$('.bgParallax').each(function(){
		var $obj = $(this),
				offsetTop = this.offsetTop;
	 
		$(window).scroll(function() {
			if($(window).scrollTop() >= offsetTop - $(window).height()) {
				var yPos = -($(window).scrollTop() / $obj.data('speed')) + offsetTop; 
				var bgpos = '50% '+ yPos + 'px';
				$obj.css('background-position', bgpos );			
			}
		}); 
	});

	$("select").each(function(){
		$(this).fancySelect();
	});

	if($('#telefone').length > 0) {
		$('#telefone').mask('(00) 0000-0000', {placeholder: "(__) ____-____"});
	}
	//
  // Tabs
  //
  // <div class="tabs-container" id="container">
  //  <div id="target"></div>
  // </div>
  // <ul class="tabs" data-target="#container">
  //  <li class="tab-item">
  //    <a href="#target" data-action="tab"></a>
  //  </li>
  // </ul>
  // 

  $(".tabs-container > div").hide();
  $(".tabs-container > div:first-child").show().addClass('current');
  var firstTab = $(".tabs li:first-child");
  if(firstTab.length > 0) {
  	var halfTabWidth = firstTab.width() / 2;
	  firstTab.addClass('active');
	  // Posicionar seta embaixo da aba ativa:
	  // firstTab.find('a').position().left + halfTabWidth - 10
	  // posição relativa ao pai + metade da largura do elemento + largura da seta do bg
	  firstTab.parent().css('background-position', (firstTab.find('a').position().left + halfTabWidth - 10) + 'px bottom');
	  $("[data-action=tab]").click(function(e){
	    e.preventDefault();
	    var el     = $(this),
	    target = el.attr("href"),
	    container = el.parents(".tabs").data("target");
	    if(("#" + $(container + " > .current").attr("id")) != target){
	      el.parents(".tabs").find(".active").removeClass("active");
	      el.parent().addClass("active");
	      $(container + " > .current").removeClass("current").fadeOut(400, function(){
	        $(target).fadeIn(400, function(){
	          $(this).addClass("current");
	        });
	      });
	      var width = el.position().left + el.width()/2 - 10;
	      el.parents('.tabs').css('background-position', width + 'px bottom');
	    }
	  });
	}

  // lightbox 
  $("[rel^='prettyPhoto']").prettyPhoto({
    social_tools: false,
    show_title: true,
    autoplay: false,
    default_width: 890
  });

  $(".js-open-lightbox").click(function(e){
    e.preventDefault();
    api_images = $(this).data("images").substring(0, $(this).data("images").length-1).split(';');
    api_titles = $(this).data("titles").substring(0, $(this).data("titles").length-1).split(';');
    api_descriptions = $(this).data("descriptions").substring(0, $(this).data("descriptions").length-1).split(';');
    $.prettyPhoto.open(api_images,api_titles,api_descriptions);
  });


  $("body").on("click",".close-lightbox", function(e) {
  	e.preventDefault();
		$.prettyPhoto.close();
  });

  $(".js-open-portfolio-lightbox").click(function(e){
  	e.preventDefault();
  	var target = $(this).attr("href");
  	// $(".lightbox-content").html($(target).html());
  	// $(".lightbox").show(500);
  	$(target).css("top", "0").addClass('lightbox-opened');
  	$(".header-nav").css("z-index", "0");
  	$("body").css("overflow-y", "hidden");
  	$(".carousel-post").slick();
  });


  $(".js-close-lightbox").click(function(e){
  	e.preventDefault();
  	$(".lightbox-opened").css("top", "100%");
  	$(".header-nav").css("z-index", "20");
  	setTimeout(function(){
  		$("body").css("overflow-y", "scroll");
  	}, 400);
		
  	// $(".lightbox").hide();
  });


  $(".client-card").click(function(){
  	$(".card-active").removeClass("card-active");
  	$(this).addClass('card-active');
  });

  // $(".client-info").mouseleave(function(event) {
  // 	$(".card-active").removeClass('card-active');
  // });

});