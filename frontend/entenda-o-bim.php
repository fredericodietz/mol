<?php 
$page = "bim";
include '_header.php'; ?>
<div class="content txt-center has-menu">
	<div class="content-menu clearfix">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-quarter"><a href="entenda-o-bim.php" class="content-menu-link icon-entenda-bim active"><span class="inner-icon"></span>Entenda o BIM</a></li>
					<li class="content-menu-item one-quarter"><a href="por-que-bim.php" class="content-menu-link icon-por-que-bim"><span class="inner-icon"></span>Por que BIM?</a></li>
					<li class="content-menu-item one-quarter"><a href="pense-mol-pense-bim.php" class="content-menu-link icon-pense-bim"><span class="inner-icon"></span>pense mol! pense bim</a></li>
					<li class="content-menu-item one-quarter"><a href="construa-com-bim.php" class="content-menu-link icon-construa"><span class="inner-icon"></span>Construa com BIM</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section>
		<div class="container_12 clearfix">
			<div class="grid_10 push_1">
				<h2>Entenda o que é BIM</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nisi consequatur illo reiciendis cumque veniam animi libero qui, quis voluptatem in, odit iure, provident distinctio ea atque debitis doloremque temporibus?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nisi consequatur illo reiciendis cumque veniam animi libero qui, quis voluptatem in, odit iure, provident distinctio ea atque debitis doloremque temporibus?</p>
			</div>
		</div>
	</section>
	<section class="dimensions bg-dark">
		<div class="container_12 clearfix">
			<div class="grid_6">
				<img src="images/display.png" alt="">
				<h3>BIM não é um software</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nisi consequatur illo reiciendis cumque veniam animi libero qui, quis voluptatem in, odit iure, provident distinctio ea atque debitis doloremque temporibus?</p>
			</div>
			<div class="grid_6">
				<img src="images/project.png" alt="">
				<h3>Vai além do ambiente de projeto</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nisi consequatur illo reiciendis cumque veniam animi libero qui, quis voluptatem in, odit iure, provident distinctio ea atque debitis doloremque temporibus?</p>
			</div>
		</div>
		
	</section>
	<section class="about-bim"><h3>Dimensões de Desenvolvimento BIM</h3>
		<div class="container_12 clearfix">
			<ul class="tabs dimensions-list" data-target="#dimensions">
				<li class="tab-item dimension-tab">
					<a href="#dimension-2d" data-action="tab">2D</a>
				</li>
				<li class="tab-item dimension-tab">
					<a href="#dimension-3d" data-action="tab">3D</a>
				</li>
				<li class="tab-item dimension-tab">
					<a href="#dimension-4d" data-action="tab">4D</a>
				</li>
				<li class="tab-item dimension-tab">
					<a href="#dimension-5d" data-action="tab">5D</a>
				</li>
				<li class="tab-item dimension-tab">
					<a href="#dimension-6d" data-action="tab">6D</a>
				</li>
				<li class="tab-item dimension-tab">
					<a href="#dimension-7d" data-action="tab">7D</a>
				</li>
			</ul>
			<div class="tabs-container dimensions-container" id="dimensions">
				<div id="dimension-2d" class="grid_10 push_1 dimension-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio minima ea quam ipsa velit natus optio, ab adipisci. Vero voluptates blanditiis molestiae error rem. Dolorem delectus ullam exercitationem pariatur quis?</p>
				</div>
				<div id="dimension-3d" class="grid_10 push_1 dimension-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio minima ea quam ipsa velit natus optio, ab adipisci. Vero voluptates blanditiis molestiae error rem. Dolorem delectus ullam exercitationem pariatur quis?</p>
				</div>
				<div id="dimension-4d" class="grid_10 push_1 dimension-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus fugiat, ipsa, dolorem facere neque molestiae inventore, incidunt, quaerat pariatur ut labore. Facilis, quasi et eos excepturi reprehenderit minus, placeat magnam.</p>
				</div>
				<div id="dimension-5d" class="grid_10 push_1 dimension-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius sapiente sunt explicabo dicta at deserunt praesentium iusto provident quisquam excepturi omnis aspernatur quae libero, est corporis itaque labore nihil sint!</p>
				</div>
				<div id="dimension-6d" class="grid_10 push_1 dimension-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit eius quos sed, reiciendis modi corrupti, ipsa eveniet officia veniam vel, dignissimos. Autem, perferendis quibusdam architecto iure illum consequatur dolore aliquid!</p>
				</div>
				<div id="dimension-7d" class="grid_10 push_1 dimension-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa a quas, repellat dolor voluptatem quod rem neque illum iste dicta exercitationem voluptatibus et doloremque, sit odit perferendis cum recusandae fuga.</p>
				</div>
			</div>
		</div>
	</section>
</div>
<section class="bg-orange txt-center">
	<a href="" title="projetos bim da mol!" class="button">projetos bim da mol!<span class="go"></span></a>
</section>
<?php include '_footer.php'; ?>