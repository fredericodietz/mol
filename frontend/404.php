<?php
$page = "error404";
include '_header.php'; ?>
<section>
	<div class="container_12 clearfix">
		<div class="grid_10 push_1 txt-center">
			<img src="images/logo.jpg" alt="">
			<h1>A página que você procura não existe.</h1>
			<p>Você pode <a href="" title="ir para página inicial">voltar para home</a>, navegar por nossos <a href="" title="conheça nossos serviços">serviços</a> ou entrar em <a href="" title="entre em contato com a nossa equipe">contato</a> com a nossa equipe.</p>
		</div>
	</div>
	<div class="plug plug-m">
		<img src="images/plug-m.jpg" alt="">
	</div>
	<div class="plug plug-f">
		<img src="images/plug-f.jpg" alt="">
	</div>
</section>
<?php include '_footer.php'; ?>