<?php include '_header.php'; ?>
<div class="content txt-center has-menu">
	<div class="content-menu clearfix">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-third"><a href="a-mol.php" class="content-menu-link icon-historia"><span class="inner-icon"></span>Nossa História</a></li>
					<li class="content-menu-item one-third"><a href="3-pilares.php" class="content-menu-link icon-pilares active"><span class="inner-icon"></span>3 Pilares de Qualidade</a></li>
					<li class="content-menu-item one-third"><a href="equipe.php" class="content-menu-link icon-equipe"><span class="inner-icon"></span>Equipe</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section>
		<div class="container_12 clearfix">
			<div class="grid_10 push_1">
				<h2>3 Pilares de Qualidade</h2>
				<p>Os três pilares de qualidade mol! formam o ambiente necessário para que nosso produto seja desenvolvido com todos os valores que o tornam um produto mol!</p>
			</div>
		</div>
	</section>
	<section class="first-section clearfix bgParallax" data-speed="1.5">
		<div class="container_12">
			<div class="grid_10 push_1">
				<span class="number">1</span>
				<h3>Abordagem de parceria</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae labore reiciendis enim quaerat cumque earum ullam, eligendi exercitationem, quisquam provident libero doloribus fugit veniam ad commodi voluptatum mollitia dignissimos vel. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod ab excepturi debitis necessitatibus veniam eum, placeat maiores sit fuga architecto, eaque quis eos laborum alias expedita molestiae distinctio accusamus impedit?</p>
			</div>
		</div>
	</section>
	<section class="second-section clearfix">
		<div class="container_12">
			<div class="grid_10 push_1">
				<span class="number">2</span>
				<h3>Ciclo de vida do design</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod ab excepturi debitis necessitatibus veniam eum, placeat maiores sit fuga architecto, eaque quis eos laborum alias expedita molestiae distinctio accusamus impedit?</p>
			</div>
			<img src="images/ciclo_de_vida.png" alt="">
		</div>
	</section>
	<section class="third-section clearfix">
		<div class="container_12">
			<div class="grid_10 push_1">
				<span class="number">3</span>
				<h3>Navegador de Gestão</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt explicabo aut vitae, reprehenderit corporis deserunt officia fugiat rem quibusdam debitis ullam fugit quos ipsa consequuntur eum totam inventore neque provident?</p>
			</div>
		</div>
	</section>
</div>
<?php include '_footer.php'; ?>