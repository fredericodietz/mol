<?php if ($page != "error404") { ?>
<div class="pre-footer">
	<section class="bg-dark txt-center">
		<a href="" title="vamos trabalhar juntos?" class="button">vamos trabalhar juntos?<span class="go"></span></a>
	</section>
	<section class="clients section-carousel">
		<div class="container_12 clearfix">
			<h3>nossos clientes</h3>
			<div class="carousel grid_12 carousel-clients">
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
			</div>

		</div>
	</section>
	<section class="partners section-carousel bg-orange">
		<div class="container_12 clearfix">
			<h3>Parceiros da mol!</h3>
			<div class="carousel grid_12 carousel-partners">
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
			</div>
		</div>
	</section>
</div>
<footer class="main-footer">
	<div class="container_12">
		<a href="" title=""><img src="images/logo-footer.png" alt=""></a>
		<span class="footer-icon icon-phone">(62) 4456 4098</span>
		<span class="footer-icon icon-map">Rua C 104 Qd 317 Lt 02 Loja 1 Goiânia - GO</span>
		<a href="" title="Curta nossa página" class="footer-icon icon-facebook" target="_blank">Curta nossa página</a>
	</div>
	<a href="http://www.katon.com.br/?utm_source=site%20de%20cliente&utm_medium=assinatura&utm_campaign=mol" target="_blank" class="assinatura-katon">
		<img src="images/katon.png">
	</a>
</footer>
</div>
<script src="bower_components/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="bower_components/slick-carousel/slick/slick.js" type="text/javascript"></script>
<script src="bower_components/jquery-prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<!-- <script src="bower_components/jquery.stellar/jquery.stellar.js" type="text/javascript"></script> -->

<?php if(isset($page) && $page == "home") { ?>

<script src="bower_components/vide/dist/jquery.vide.min.js"></script>
<?php } ?>
<script src="js/main.js"></script>
<?php if(isset($contato)) { ?>
<script>
	function initialize() {

		var styles = [
		{
			"featureType": "landscape",
			"stylers": [
			{ "visibility": "on" },
			{ "color": "#c0c7cd" }
			]
		},{
			"featureType": "road",
			"stylers": [
			{ "visibility": "simplified" },
			{ "color": "#ffffff" }
			]
		},{
			"featureType": "road",
			"elementType": "labels.text.fill",
			"stylers": [
			{ "visibility": "simplified" },
			{ "color": "#92a4b6" }
			]
		},{
			"featureType": "road",
			"elementType": "labels.text.stroke",
			"stylers": [
			{ "color": "#ffffff" }
			]
		},{
			"featureType": "poi",
			"stylers": [
			{ "visibility": "off" }
			]
		},{
			"featureType": "water",
			"stylers": [
			{ "color": "#e2f1f8" }
			]
		},{
			"featureType": "administrative",
			"elementType": "labels.text",
			"stylers": [
			{ "color": "#808080" },
			{ "visibility": "simplified" }
			]
		}
		];

		var styledMap = new google.maps.StyledMapType(styles,
			{name: "Styled Map"});

		var myLatLng = new google.maps.LatLng(-16.713995, -49.287962);
		var mapOptions = {
			zoom: 15,
			scrollwheel: false,
			center: myLatLng,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			}
		};
		var map = new google.maps.Map(document.getElementById('map_canvas'),
			mapOptions);
		var iconBase =  'images/';
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			icon: iconBase + 'location.png'
		});

		map.mapTypes.set('map_style', styledMap);
		map.setMapTypeId('map_style');
	}
	initialize();
</script>
<?php } else if($page == "bim") { ?>
		<script src="js/TweenLite.min.js"></script>
		<script src="js/EasePack.min.js"></script>
		<script src="js/rAF.js"></script>
		<script src="js/demo-1.js"></script>
<?php
	}
} else { ?>
</div>
<?php } ?>
</body>
</html>