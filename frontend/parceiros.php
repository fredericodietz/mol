<?php include '_header.php'; ?>
<div class="content has-menu parent-seja-parceiro">
	<div class="content-menu clearfix">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-third"><a href="" class="content-menu-link icon-escritorios active"><span class="inner-icon"></span>Escritórios de Arquitetura</a></li>
					<li class="content-menu-item one-third"><a href="" class="content-menu-link icon-clientes"><span class="inner-icon"></span>Clientes</a></li>
					<li class="content-menu-item one-third"><a href="" class="content-menu-link icon-equipe"><span class="inner-icon"></span>Time Mol</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section class="container_12 clearfix">
		<div class="grid_12">
			<h2>Parceiro Externo</h2>
			<p>empenhamos um esforço para que todos os parceiros da mol! (ou, seja, todos os envolvidos com nosso negócio como clientes, colaboradores, fornecedores, etc) sejam contemplados para que tenhamos um trabalho sempre pautado na colaboração, fluência na linguagem de cada envolvido e um respeito por todos aqueles que se empenham conosco para resultados de excelência.</p>
			<form action="" method="get">
				<div class="grid_6 alpha omega">
					<div class="grid_4 alpha">
						<label for="nome">Nome</label>
						<input type="text">
					</div>
					<div class="grid_2 alpha">
						<label for="telefone">Telefone</label>
						<input type="text">
					</div>
					<div class="grid_3 alpha">
						<label for="email">Email</label>
						<input type="text">
					</div>
					<div class="grid_3 alpha">
						<label for="empresa">Empresa</label>
						<input type="text">
					</div>
				</div>
				<div class="textarea-fix">
					<label for="msg">Mensagem</label>
					<textarea name="" style="height: 128px;"></textarea>
				</div>
				<div class="clear"></div>
				<input type="submit" name="" value="enviar" class="submit-button">
			</form>
		</div>
	</section>
</div>
<?php include '_footer.php'; ?>