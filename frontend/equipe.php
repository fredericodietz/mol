<?php include '_header.php'; ?>
<div class="content txt-center has-menu">
	<div class="content-menu clearfix">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-third"><a href="a-mol.php" class="content-menu-link icon-historia"><span class="inner-icon"></span>Nossa História</a></li>
					<li class="content-menu-item one-third"><a href="3-pilares.php" class="content-menu-link icon-pilares"><span class="inner-icon"></span>3 Pilares de Qualidade</a></li>
					<li class="content-menu-item one-third"><a href="equipe.php" class="content-menu-link icon-equipe active"><span class="inner-icon"></span>Equipe</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section>
		<div class="container_12 clearfix">
			<h2>Equipe</h2>
			<div class="grid_10 push_1">
				<p>Os nossos parceiros internos formam o ambiente necessário para que nosso produto seja desenvolvido com todos os valores que o tornam um produto mol!</p>
			</div>
			<div class="grid_12 cards members">
				<div class="card js-flip">
					<img src="images/temp/carlao.jpg" alt="" class="js-flip-image" data-image="images/temp/carlao-back.jpg">
					<div class="flip-cover">
						<img src="images/temp/carlao-back.jpg" alt="">
						<div class="member-info">
							<p class="member-name">Nome</p>
							<p class="member-department">Departamento</p>
						</div>
					</div>
				</div>
				<div class="card js-flip">
					<img src="images/temp/vinicius.jpg" alt="" class="js-flip-image" data-image="images/temp/vinicius.jpg">
					<div class="flip-cover">
						<img src="images/temp/vinicius.jpg" alt="">
						<div class="member-info">
							<p class="member-name">Nome</p>
							<p class="member-department">Departamento</p>
						</div>
					</div>
				</div>
				<div class="card js-flip">
					<img src="images/temp/didi.jpg" alt="" class="js-flip-image" data-image="images/temp/didi-back.jpg">
					<div class="flip-cover">
						<img src="images/temp/didi-back.jpg" alt="">
						<div class="member-info">
							<p class="member-name">Nome</p>
							<p class="member-department">Departamento</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include '_footer.php'; ?>