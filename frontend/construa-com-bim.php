<?php 
$page = "bim";
include '_header.php'; ?>
<div class="content has-menu">
	<div class="content-menu clearfix">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-quarter"><a href="entenda-o-bim.php" class="content-menu-link icon-entenda-bim"><span class="inner-icon"></span>Entenda o BIM</a></li>
					<li class="content-menu-item one-quarter"><a href="por-que-bim.php" class="content-menu-link icon-por-que-bim"><span class="inner-icon"></span>Por que BIM?</a></li>
					<li class="content-menu-item one-quarter"><a href="pense-mol-pense-bim.php" class="content-menu-link icon-pense-bim"><span class="inner-icon"></span>pense mol! pense bim</a></li>
					<li class="content-menu-item one-quarter"><a href="construa-com-bim.php" class="content-menu-link icon-construa active"><span class="inner-icon"></span>Construa com BIM</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section>
		<div class="container_12 clearfix">
			<div class="grid_12">
			<h2>Contrua com BIM</h2>
			<p>Entre em contato com a Mol! Engenharia, teremos prazer em esclarecer dúvidas e mostrar o que já fizemos com a versatilidade da metodologia BIM!</p>
				<form action="" method="get">
					<div class="grid_6 alpha omega">
						<div class="grid_4 alpha">
							<label for="nome">Nome</label>
							<input type="text">
						</div>
						<div class="grid_2 alpha">
							<label for="telefone">Telefone</label>
							<input type="text">
						</div>
							<label for="email">Email</label>
							<input type="text" class="full-input">
							<label for="empresa">Empresa</label>
							<input type="text" class="full-input">
					</div>
					<div class="textarea-fix">
						<label for="msg">Mensagem</label>
						<textarea name="" style="height: 216px;"></textarea>
					</div>
					<div class="clear"></div>
					<input type="submit" name="" value="enviar" class="submit-button">
				</form>
			</div>
		</div>
	</section>
</div>
<?php include '_footer.php'; ?>