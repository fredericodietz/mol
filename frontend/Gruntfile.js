module.exports = function(grunt) {
	'use strict';
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			options: {
				livereload: true
			},
			css: {
				files: 'src/sass/style.scss',
				tasks: 'compass:dev'
			},
			js: {
				files: 'js/main.js',
				tasks: 'jshint'
			},
			php: {
				files: '*.php'
			},
			// html: {
			// 	files: '*.html'
			// }
		},
		compass: {
			dev: {
				options: {
					sassDir: 'src/sass',
					cssDir: 'css/',
					specify: 'src/sass/style.scss',
					httpImagesPath: '../images/',
					generatedImagesDir: "images/sprites/",
					httpGeneratedImagesPath: "../images/sprites",
					httpFontsPath: "fonts/",
					outputStyle: 'expanded'
				}
			},
			dist: {
				options: {
					banner :
						"/* \n" +
						"Theme Name: <%= pkg.description %>\n" +
						"Version: 1.0\n" +
						"Author: <%= pkg.author %>\n" +
						"Author URI: http://katon.com.br\n" +
						"*/\n",
					sassDir: 'src/sass/',
					cssDir: '../site/wp-content/themes/tema/',
					specify: 'src/sass/style.scss',
					httpGeneratedImagesPath: "images/",
					outputStyle: 'compressed',
					httpImagesPath: 'images/',
					httpFontsPath: "css/fonts/"
				}
			}
		},
		jshint: {
			files: ['Gruntfile.js', 'js/main.js'],
			options: {
				globals: {
					jQuery: true
				}
			}
		},
	  concat: {
      options: {
        separator: ';\n'
      },
      dist: {
        src: ['!js/plugins.js', 'bower_components/slick-carousel/slick/slick.js', 'bower_components/jquery-prettyPhoto/js/jquery.prettyPhoto.js'],
        dest: 'js/plugins.js'
      }
    },
		uglify: {
			my_target: {
				options: {
					banner: "// <%= pkg.author %> \n"
				},
				files: {
					'../site/wp-content/themes/tema/js/main.js': ['js/main.js'],
	  			'js/plugins.min.js': 'js/plugins.js',
	  			'js/fancySelect.min.js': 'bower_components/fancyselect/fancySelect.js'
				}
			}
		},
	  copy: {
	  	main: {
	  		files: [
		  		{expand: true, src: 'js/plugins.min.js', dest: "../site/wp-content/themes/tema/"},
		  		{expand: true, src: 'js/fancySelect.min.js', dest: "../site/wp-content/themes/tema/"},
		  		{expand: true, src: 'js/jquery.mask.min.js', dest: "../site/wp-content/themes/tema/"},
		  		{expand: false, src: 'bower_components/html5shiv/dist/html5shiv.min.js', dest: "../site/wp-content/themes/tema/js/html5shiv.min.js"},
		  		{expand: false, src: 'bower_components/vide/dist/jquery.vide.min.js', dest: "../site/wp-content/themes/tema/js/jquery.vide.min.js"},
		  		{expand: false, src: 'bower_components/slick-carousel/slick/slick.css', dest: "../site/wp-content/themes/tema/css/slick.css"},
		  		{expand: false, src: 'bower_components/jquery-prettyPhoto/css/prettyPhoto.css', dest: "../site/wp-content/themes/tema/css/prettyPhoto.css"}
		  	]
	  	},
	  	images: {
	  		files: [
	  			{expand: true, src: ['images/**', '!images/temp/**'], dest: "../site/wp-content/themes/tema/"}
	  		]
	  	}
	  }
	});

	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.registerTask('default', ['watch', ]);
	grunt.registerTask('build', ['compass:dist', 'jshint', 'concat', 'uglify', 'copy:main']);
	grunt.registerTask('images', ['copy:images']);
	grunt.registerTask('buildjs', ['concat', 'uglify']);
};