<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>mol engenharia</title>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="bower_components/slick-carousel/slick/slick.css">
	<link rel="stylesheet" href="bower_components/jquery-prettyPhoto/css/prettyPhoto.css">
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
		<script src="bower_components/html5shiv/dist/html5shiv.min.js" type="text/javascript"></script>
		<![endif]-->
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	</head>
	<body class="<?php echo isset($page) ? $page : ""; ?>">
		<div class="wrapper">
			<?php if ($page != "error404") { ?>
			<div class="header-nav <?php echo $page == "blog" ? "header-white" : "header-transparent"; ?>">
				<div class="container_12">
					<h1 class="logo">
						<a href="index.php" title="">mol! engenharia</a>
					</h1>
					<ul class="inline-list header-menu">
						<li class="menu-item current-menu-item"><a href="a-mol.php" title="" class="menu-link">a mol</a></li>
						<li class="menu-item"><a href="servicos.php" title="" class="menu-link">serviços</a></li>
						<li class="menu-item"><a href="clientes.php" title="" class="menu-link">clientes</a></li>
						<li class="menu-item"><a href="portfolio.php" title="" class="menu-link">portfolio</a></li>
						<li class="menu-item"><a href="blog.php" title="" class="menu-link">blog</a></li>
						<li class="menu-item"><a href="parceiros.php" title="" class="menu-link">seja parceiro</a></li>
						<li class="menu-item"><a href="contato.php" title="" class="menu-link">contato</a></li>
					</ul>
					<a href="" title="" class="login-item">login</a>
					<a href="entenda-o-bim.php" title="" class="button-bim">conheça o bim</a>
				</div>
			</div>
			<?php if(!isset($page) || $page != "blog" && $page != "bim") { ?>
			<?php if($page == "home") { 
				echo "<div class='video'  data-vide-bg='video/video.mp4'></div>";
			} ?>

			<header class="main-header clearfix" <?php if($page != "home") { ?> style="background: url(images/temp/header.jpg) center top no-repeat;" <?php } ?>>
				<div class="intro vert-center">
					<?php if($page == "home") { ?>
					<h2 class="main-title">projetos de <span>engenharia</span><br>pensados como <span>design</span></h2>
					<a href="" class="button button-red">conheça nossos serviços</a>
					<?php } else { ?>
					<h2 class="main-title">a mol! engenharia nasceu da ideia de fazer diferente.<br>conheça mais sobre a empresa.</h2>
					<?php } ?>
				</div>
			</header><!-- /header -->
			<?php } else if($page == "bim") { ?>
			<header class="main-header clearfix large-header" id="large-header">
					<canvas id="demo-canvas"></canvas>
					<div class="intro vert-center">
						<h2 class="main-title">bim é a qualidade dos projetos da mol<br> aliados ao modelo tridimensional paramétrico</h2>
					</div>
			</header>
			<?php
		}
	} ?>