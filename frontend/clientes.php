<?php 
$page = "clientes";
include '_header.php'; ?>
<div class="content">
	<section class="container_12 clearfix">
		<div class="grid_12">
			<ul class="tabs clients-list" data-target="#clients-container">
				<li class="tab-item client-tab">
					<a href="#todos" data-action="tab">Todos os clientes</a>
				</li>
				<li class="tab-item client-tab">
					<a href="#construtoras" data-action="tab">Construtoras e Incorporadoras</a>
				</li>
				<li class="tab-item client-tab">
					<a href="#escritorios" data-action="tab">Escritórios de Arquitetura</a>
				</li>
			<li class="tab-item client-tab">
				<a href="#marca" data-action="tab">Marcas e Franquias</a>
			</li>
			</ul>
			<div class="tabs-container cards" id="clients-container">
				<div id="todos">
					<div class="card client-card">
						<div class="vert-center">
							<img src="images/temp/client.png" alt="" class="client-logo">
						</div>
						<div class="client-info">
							<h2>Merzia</h2>
							<span alt="Design de subestação" class="project-feature">
								<span class="icon-hidrossanitaria-border"></span>
							</span>
							<span alt="Design de subestação" class="project-feature">
								<span class="icon-4d-border"></span>
							</span>
							<span alt="Design de subestação" class="project-feature">
								<span class="icon-incendio-border"></span>
							</span>
							<span alt="Design de subestação" class="project-feature">
								<span class="icon-eletrica-border"></span>
							</span>
							<a href="" title="" class="view-project">ver projeto</a>
						</div>
					</div>
					<div class="card client-card">
						<div class="vert-center">
							<img src="images/temp/client2.png" alt="" class="client-logo">
						</div>
						<div class="client-info">
							<h2>Merzia</h2>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<a href="" title="" class="view-project">ver projeto</a>
						</div>
					</div>
				</div>
				<div id="escritorios">
					<div class="card client-card">
						<div class="vert-center">
							<img src="images/temp/client3.png" alt="" class="client-logo">
						</div>
						<div class="client-info">
							<h2>Merzia</h2>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<a href="" title="" class="view-project">ver projeto</a>
						</div>
					</div>
					<div class="card client-card">
						<div class="vert-center">
							<img src="images/temp/client3.png" alt="" class="client-logo">
						</div>
					</div>
					<div class="card client-card">
						<div class="vert-center">
							<img src="images/temp/client2.png" alt="" class="client-logo">
						</div>
					</div>
					<div class="card client-card">
						<div class="vert-center">
							<img src="images/temp/client.png" alt="" class="client-logo">
						</div>
					</div>
				</div>
				<div id="construtoras">
					<div class="card client-card">
						<div class="vert-center">
							<img src="images/temp/client.png" alt="" class="client-logo">
						</div>
						<div class="client-info">
							<h2>Merzia</h2>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<span alt="Design de subestação" class="project-feature">
								<img src="images/temp/project-feature.png">
							</span>
							<a href="" title="" class="view-project">ver projeto</a>
						</div>
					</div>

				</div>
			</div>
		</section>

	</div>
	<?php include '_footer.php'; ?>