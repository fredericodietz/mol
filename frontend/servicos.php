<?php include '_header.php'; ?>
<div class="content has-menu">
	<div class="content-menu clearfix">
		<nav class="container_12">
			<div class="grid_12">
				<ul class="inline-list">
					<li class="content-menu-item one-quarter"><a href="" class="content-menu-link icon-eletrica"><span class="inner-icon"><span></span>Pack de Design e Instalações Elétricas</span></a></li>
					<li class="content-menu-item one-quarter"><a href="" class="content-menu-link icon-incendio"><span class="inner-icon"><span></span>Design de Instalações de Combate a Incêndio</span></a></li>
					<li class="content-menu-item one-quarter"><a href="" class="content-menu-link icon-hidrossanitaria"><span class="inner-icon"><span></span>Design de Instalações Hidrossanitárias</span></a></li>
					<li class="content-menu-item one-quarter"><a href="" class="content-menu-link icon-4d"><span class="inner-icon"><span></span></span>Desenvolvimento 4D</a></li>
				</ul>
			</div>
		</nav>
		<div class="clear"></div>
	</div>
	<section class="container_12 clearfix service">
		<!-- <div class="grid_4"> -->
			<h2 class="icon-hidrossanitaria-big"><span></span>Pack de Design de Instalações Elétricas</h2>
		<!-- </div> -->
		<div class="grid_8">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam quia eum incidunt et dicta sapiente, quidem ad numquam dolore autem tempore blanditiis, vitae quasi iusto, consequuntur libero facere similique! Quos. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam quia eum incidunt et dicta sapiente, quidem ad numquam dolore autem tempore blanditiis, vitae quasi iusto, consequuntur libero facere similique! Quos.</p>
		</div>
	</section>
	<section class="parallax bg-pack">
		<div class="container_12 clearfix">
			<h3 class="txt-center">O Pacote de Design de Instalações Elétricas contém:</h3>
			<div class="grid_6">
				<p class="item-pack icon-check">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum expedita ipsam officia voluptates, minus nemo alias.</p>
				<p class="item-pack icon-check">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum expedita ipsam officia voluptates, minus nemo alias.</p>
			</div>
			<div class="grid_6">
				<p class="item-pack icon-check">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum expedita ipsam officia voluptates, minus nemo alias.</p>
				<p class="item-pack icon-check">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum expedita ipsam officia voluptates, minus nemo alias.</p>
			</div>
		</div>
	</section>
	<section>
		<div class="container_12 clearfix areas">
			<div class="grid_3">
				<h3 class="">Áreas técnicas<br>do pacote</h3>
			</div>
			<div class="grid_9">
				<div class="one-third">
					<p class="icon-temp active">Design de SPDA</p>
				</div>
				<div class="one-third">
					<p class="icon-temp active">Design de Subestação</p>
				</div>
				<div class="one-third">
					<p class="icon-temp active">Design de Infraestrutura de Comunicação &nbsp;<a href="#" class="read-more"></a></p>
				</div>
				<div class="clear"></div>
				<div class="more-area">
					<p><abbr title="Instalações Telefônicas">IT</abbr><span>Instalações <br>Telefônicas</span></p>
					<p><abbr title="Cabeamento Estruturado">CE</abbr><span>Cabeamento <br>Estruturado</span></p>
					<p><abbr title="Infraestrutura de Interfone">IF</abbr><span>Infraestrutura <br>de Interfone</span></p>
					<p><abbr title="Infraestrutura de Televisão e Antena Coletiva">AT</abbr><span>Infraestrutura de Televisão <br>e Antena Coletiva</span></p>
				<div class="clear"></div>
				</div>
				<div class="one-third">
					<p class="icon-temp active">Design de Instalações Elétricas</p>
				</div>
				<div class="one-third">
					<p class="icon-temp active">Design de Automação</p>
				</div>
				<div class="one-third">
					<p class="icon-temp active">Design de Infraestrutura de Segurança</p>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include '_footer.php'; ?>